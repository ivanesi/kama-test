// ignore_for_file: unnecessary_lambdas

import 'package:kama_test/app/features/car/cubit/car_screen_cubit.dart';
import 'package:kama_test/app/features/welcome/cubit/welcome_screen_cubit.dart';
import 'package:kama_test/app/init/app_launcher.dart';
import 'package:kama_test/app/init/sl.dart';
import 'package:kama_test/app/presentation/app.dart';
import 'package:kama_test/app/presentation/router/app_router.dart';
import 'package:flutter/material.dart';

class Initializer {
  AppLauncher get launcher => sl<AppLauncher>();

  Future<void> init() async {
    _registerServiceLocator();

    WidgetsFlutterBinding.ensureInitialized();
  }

  void _registerServiceLocator() {
    _registerApp();

    sl.registerSingleton(AppLauncher(sl()));
  }

  void _registerApp() {
    sl.registerSingleton(AppRouter());
    sl.registerSingleton(App(sl()));

    _registerBlocs();
  }

  void _registerBlocs() {
    sl.registerFactory(() => WelcomeScreenCubit(sl()));
    sl.registerFactory(() => CarScreenCubit(sl()));
  }
}
