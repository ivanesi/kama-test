import 'package:kama_test/app/presentation/app.dart';
import 'package:flutter/material.dart';

class AppLauncher {
  final App _app;

  AppLauncher(this._app);

  void run() {
    runApp(_app);
  }
}
