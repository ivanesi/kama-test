import 'package:kama_test/app/presentation/router/app_router.dart';
import 'package:flutter/material.dart';

class App extends StatelessWidget {
  final AppRouter _router;

  const App(
    this._router,
  );

  @override
  Widget build(BuildContext context) => MaterialApp(
        title: 'kama-test',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        initialRoute: _router.initialRoute,
        navigatorKey: _router.navigatorKey,
        onGenerateRoute: _router.onGenerateRoute,
        navigatorObservers: [_router.navigatorObserver],
        debugShowCheckedModeBanner: false,
      );
}
