import 'dart:async';
import 'package:kama_test/app/presentation/router/route_transition.dart';
import 'package:flutter/widgets.dart';

class AppRouteObserver extends RouteObserver<PageRoute<dynamic>> {
  final _routeTransitionStreamController = StreamController<RouteTransition>();

  late RouteTransition _lastRouteTransition;

  RouteTransition get lastRouteTransition => _lastRouteTransition;

  Stream<RouteTransition> get routeTransitionStream => _routeTransitionStreamController.stream;

  AppRouteObserver({
    required String initialRoute,
  }) {
    _routeTransitionStreamController.stream.listen((transition) => _lastRouteTransition = transition);

    final initialRouteSettings = RouteSettings(name: initialRoute);

    _routeTransitionStreamController.add(RouteTransition(
      previousSettings: initialRouteSettings,
      currentSettings: initialRouteSettings,
    ));
  }

  @override
  void didPush(Route<dynamic> route, Route<dynamic>? previousRoute) {
    super.didPush(route, previousRoute);
    if (previousRoute is PageRoute && route is PageRoute) {
      final routeTransition = RouteTransition(
        previousSettings: previousRoute.settings,
        currentSettings: route.settings,
      );
      _onRouteChanged(routeTransition);
    }
  }

  @override
  void didReplace({Route<dynamic>? newRoute, Route<dynamic>? oldRoute}) {
    super.didReplace(newRoute: newRoute, oldRoute: oldRoute);
    if (oldRoute is PageRoute && newRoute is PageRoute) {
      final routeTransition = RouteTransition(
        previousSettings: oldRoute.settings,
        currentSettings: newRoute.settings,
      );
      _onRouteChanged(routeTransition);
    }
  }

  @override
  void didPop(Route<dynamic> route, Route<dynamic>? previousRoute) {
    super.didPop(route, previousRoute);
    if (route is PageRoute && previousRoute is PageRoute) {
      final routeTransition = RouteTransition(
        previousSettings: route.settings,
        currentSettings: previousRoute.settings,
      );
      _onRouteChanged(routeTransition);
    }
  }

  void _onRouteChanged(RouteTransition transition) {
    print(transition);
    _routeTransitionStreamController.add(transition);
  }
}
