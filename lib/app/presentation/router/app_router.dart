import 'package:kama_test/app/features/car/widget/car_screen.dart';
import 'package:kama_test/app/features/fire_circle/fire_circle_screen.dart';
import 'package:kama_test/app/features/welcome/welcome_screen.dart';
import 'package:kama_test/app/presentation/router/app_route_observer.dart';
import 'package:kama_test/app/presentation/router/route_animation.dart';
import 'package:kama_test/app/presentation/router/route_config.dart';
import 'package:kama_test/app/presentation/router/route_transition.dart';
import 'package:flutter/material.dart';

part 'route_configurator.dart';
part 'routes.dart';

class AppRouter {
  static const _initialRoute = Routes.welcomeScreen;

  final _navigatorKey = GlobalKey<NavigatorState>();
  final _appRouteObserver = AppRouteObserver(initialRoute: _initialRoute);
  final _routeConfigurator = RouteConfigurator();

  NavigatorState? get _navigatorState => _navigatorKey.currentState;

  String get initialRoute => _initialRoute;

  GlobalKey<NavigatorState> get navigatorKey => _navigatorKey;

  NavigatorObserver get navigatorObserver => _appRouteObserver;

  String? get currentRouteName => _appRouteObserver.lastRouteTransition.currentSettings.name;

  RouteTransition get lastRouteTransition => _appRouteObserver.lastRouteTransition;

  Stream<RouteTransition> get routeTransitionStream => _appRouteObserver.routeTransitionStream;

  bool get canPop => _navigatorState?.canPop() ?? false;

  BuildContext? get _currentContext => _navigatorState?.overlay?.context;

  void newRootScreen(
    String routeName, [
    Object? arguments,
  ]) {
    print('newRootScreen($routeName)');

    _navigatorState?.pushNamedAndRemoveUntil(
      routeName,
      (_) => false,
      arguments: arguments,
    );
  }

  void push(
    String routeName, [
    Object? arguments,
  ]) {
    print('push($routeName)');

    _navigatorState?.pushNamed(
      routeName,
      arguments: arguments,
    );
  }

  void pushReplacement(
    String routeName, [
    Object? arguments,
  ]) {
    print('pushReplacement($routeName)');

    _navigatorState?.pushReplacementNamed(
      routeName,
      arguments: arguments,
    );
  }

  Future<dynamic> pushAsync(
    String routeName, [
    Object? arguments,
  ]) async {
    print('pushAsync($routeName)');

    await _navigatorState?.pushNamed(
      routeName,
      arguments: arguments,
    );
  }

  void pop<T extends Object>([T? result]) {
    print('pop($result)');

    final navigatorState = _navigatorState;
    if (navigatorState == null) {
      return;
    }

    if (navigatorState.canPop() == false) {
      return;
    }

    navigatorState.pop<T>(result);
  }

  void popUntil(String routeName) {
    print('popUntil($routeName)');

    _navigatorState?.popUntil(ModalRoute.withName(routeName));
  }

  void popUntilRoot() {
    print('popUntilRoot()');

    final navigatorState = _navigatorState;
    if (navigatorState == null) {
      return;
    }

    navigatorState.popUntil((route) => navigatorState.canPop() == false);
  }

  Future<bool?> openAlertDialog({
    required AlertDialog dialog,
    bool barrierDismissible = true,
  }) =>
      openDialog<bool>(
        dialog: dialog,
        barrierDismissible: barrierDismissible,
      );

  Future<T?> openDialog<T>({
    required Widget dialog,
    bool barrierDismissible = true,
  }) async {
    final context = _currentContext;
    if (context == null) {
      return null;
    }

    return showDialog<T>(
      context: context,
      builder: (context) => WillPopScope(
        onWillPop: () => Future.value(barrierDismissible),
        child: Dialog(
          backgroundColor: Colors.white,
          insetPadding: const EdgeInsets.symmetric(horizontal: 16),
          shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(8)),
          ),
          child: dialog,
        ),
      ),
      barrierDismissible: barrierDismissible,
      barrierColor: Colors.grey,
    );
  }

  Route<dynamic> onGenerateRoute(RouteSettings routeSettings) {
    print('onGenerateRoute($routeSettings)');

    final config = _routeConfigurator.getConfig(routeSettings);

    switch (config.animation) {
      case RouteAnimation.fadeIn:
        return PageRouteBuilder<dynamic>(
          settings: routeSettings,
          pageBuilder: (context, animation, secondaryAnimation) => config.screen,
          transitionDuration: const Duration(milliseconds: 250),
          transitionsBuilder: (context, animation, secondAnimation, child) => FadeTransition(
            opacity: animation,
            child: child,
          ),
          maintainState: config.maintainState,
          fullscreenDialog: config.fullscreenDialog,
        );
      case RouteAnimation.system:
        return MaterialPageRoute<dynamic>(
          settings: routeSettings,
          builder: (context) => config.screen,
          maintainState: config.maintainState,
          fullscreenDialog: config.fullscreenDialog,
        );
    }
  }
}
