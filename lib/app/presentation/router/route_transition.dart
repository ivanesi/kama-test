import 'package:flutter/widgets.dart';

class RouteTransition {
  final RouteSettings previousSettings;
  final RouteSettings currentSettings;

  RouteTransition({
    required this.previousSettings,
    required this.currentSettings,
  });

  @override
  String toString() => 'RouteTransition{previous: ${previousSettings.name}, current: ${currentSettings.name}}';
}
