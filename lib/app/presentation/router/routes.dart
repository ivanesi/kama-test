part of 'app_router.dart';

class Routes {
  static const welcomeScreen = 'welcomeScreen';
  static const carScreen = 'carScreen';
  static const fireCircleScreen = 'fireCircleScreen';
}
