import 'package:kama_test/app/presentation/router/route_animation.dart';
import 'package:flutter/widgets.dart';

class RouteConfig {
  final Widget screen;
  final RouteAnimation animation;

  /// Set to false, if screen must be updated after pop on it
  ///
  /// More on [ModalRoute.maintainState]
  final bool maintainState;

  /// Set to true, if screen must be opened with dialog animation
  ///
  /// More on [PageRoute.fullscreenDialog]
  final bool fullscreenDialog;

  RouteConfig({
    required this.screen,
    this.animation = RouteAnimation.system,
    this.maintainState = true,
    this.fullscreenDialog = false,
  });
}
