part of 'app_router.dart';

class RouteConfigurator {
  RouteConfig getConfig(RouteSettings routeSettings) {
    final name = routeSettings.name;
    final args = routeSettings.arguments;

    switch (name) {
      case Routes.welcomeScreen:
        return RouteConfig(
          screen: const WelcomeScreen(),
          animation: RouteAnimation.fadeIn,
        );

      case Routes.carScreen:
        return RouteConfig(
          screen: const CarScreen(),
          animation: RouteAnimation.fadeIn,
        );

      case Routes.fireCircleScreen:
        return RouteConfig(
          screen: const FireCircleScreen(),
          animation: RouteAnimation.fadeIn,
        );

      default:
        return RouteConfig(
          screen: Scaffold(body: Center(child: Text('route not found: ${routeSettings.name}'))),
          animation: RouteAnimation.fadeIn,
        );
    }
  }
}
