import 'package:flutter/material.dart';
import 'package:kama_test/app/features/fire_circle/box_painter.dart';
import 'dart:math';

import 'package:kama_test/app/features/fire_circle/circle_painter.dart';

class Circle extends StatefulWidget {
  final Widget child;
  const Circle({
    required this.child,
  });

  @override
  State<Circle> createState() => _CircleState();
}

class _CircleState extends State<Circle> with SingleTickerProviderStateMixin {
  late final Animation<double> animation;
  late final AnimationController controller;

  @override
  void initState() {
    controller = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 5),
    );

    final rotationTween = Tween(begin: -pi, end: pi);

    animation = rotationTween.animate(controller)
      ..addListener(() {
        setState(() {});
      })
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          controller.repeat();
        } else if (status == AnimationStatus.dismissed) {
          controller.forward();
        }
      });

    controller.forward();

    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => CustomPaint(
        foregroundPainter: BoxOnArcPainter(animation.value),
        painter: const CirclePainter(),
        child: widget.child,
      );
}
