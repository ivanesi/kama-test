import 'package:flutter/material.dart';
import 'dart:ui' as ui;

class FirePainter extends CustomPainter {
  final double animatedValue;

  FirePainter({
    required this.animatedValue,
  });

  @override
  void paint(Canvas canvas, Size size) {
    final petalBrush = Paint()
      ..color = const Color.fromARGB(255, 33, 150, 243)
      ..style = PaintingStyle.fill
      ..strokeWidth = 1;

    petalBrush.shader = ui.Gradient.linear(
        Offset(size.width * 0.50, size.height * 0.71),
        Offset(size.width * 0.50, size.height * 0.19),
        [const Color.fromARGB(255, 238, 65, 12), const Color.fromARGB(171, 255, 204, 0)],
        [animatedValue, 1.00]);

    final petal = _createPetal(size);
    canvas.translate(-30, 40);
    canvas.drawPath(petal, petalBrush);

    canvas.drawShadow(petal, const Color.fromARGB(255, 255, 250, 242), 10 - animatedValue * 3, false);
  }

  Path _createPetal(Size size) {
    final path = Path();
    path.moveTo(size.width * 0.5833333, size.height * 0.2157143);

    //right
    path.quadraticBezierTo(
        size.width * 0.5015250, size.height * 0.3955000, size.width * 0.4996917, size.height * 0.4700571);

    //bottom
    path.cubicTo(size.width * 0.5001333, size.height * 0.5277714, size.width * 0.5448000, size.height * 0.5724000,
        size.width * 0.5833333, size.height * 0.5742857);

    //left
    path.cubicTo(size.width * 0.6221417, size.height * 0.5743571, size.width * 0.6664250, size.height * 0.5254000,
        size.width * 0.6665333, size.height * 0.4673857);

    //top
    path.quadraticBezierTo(
        size.width * 0.6672500, size.height * 0.3957714, size.width * 0.5833333, size.height * 0.2157143);
    path.close();
    return path;
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => true;
}
