import 'dart:math';

import 'package:flutter/material.dart';

class CirclePainter extends CustomPainter {
  const CirclePainter();

  @override
  void paint(Canvas canvas, Size size) {
    final circleBrush = Paint();
    circleBrush.strokeWidth = 2.0;
    circleBrush.color = Colors.red;
    circleBrush.style = PaintingStyle.stroke;

    final center = Offset(size.width / 2, size.height / 2);
    final radius = min(size.width / 2.2, size.height / 2.2);

    canvas.drawCircle(center, radius, circleBrush);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
