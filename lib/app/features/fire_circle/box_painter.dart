import 'dart:math';

import 'package:flutter/material.dart';

class BoxOnArcPainter extends CustomPainter {
  final double radians;

  BoxOnArcPainter(this.radians);

  @override
  void paint(Canvas canvas, Size size) {
    final rectBrush = Paint();
    rectBrush.style = PaintingStyle.fill;

    final center = Offset(size.width / 2, size.height / 2);
    final radius = min(size.width / 2.2, size.height / 2.2);

    const rect4Size = Size(20, 20);
    final rect4OnCircle = Offset(
      radius * cos(radians - 0.35) + center.dx - rect4Size.width / 2,
      radius * sin(radians - 0.35) + center.dy - rect4Size.height / 2,
    );
    rectBrush.color = Colors.red.withOpacity(0.15);
    canvas.drawRect(rect4OnCircle & rect4Size, rectBrush);

    const rect3Size = Size(25, 25);
    final rect3OnCircle = Offset(
      radius * cos(radians - 0.25) + center.dx - rect3Size.width / 2,
      radius * sin(radians - 0.25) + center.dy - rect3Size.height / 2,
    );
    rectBrush.color = Colors.red.withOpacity(0.3);
    canvas.drawRect(rect3OnCircle & rect3Size, rectBrush);

    const rect2Size = Size(30, 30);
    final rect2OnCircle = Offset(
      radius * cos(radians - 0.15) + center.dx - rect2Size.width / 2,
      radius * sin(radians - 0.15) + center.dy - rect2Size.height / 2,
    );
    rectBrush.color = Colors.red.withOpacity(0.6);
    canvas.drawRect(rect2OnCircle & rect2Size, rectBrush);

    const mainRectSize = Size(40, 40);
    final rectOnCircle = Offset(
      radius * cos(radians) + center.dx - mainRectSize.width / 2,
      radius * sin(radians) + center.dy - mainRectSize.height / 2,
    );
    rectBrush.color = Colors.red;
    canvas.drawRect(rectOnCircle & mainRectSize, rectBrush);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}
