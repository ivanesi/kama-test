import 'dart:math';

import 'package:flutter/material.dart';

class Sparks extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SparksState();
}

class _SparksState extends State<Sparks> with SingleTickerProviderStateMixin {
  late final AnimationController _controller;
  final List<Bubble> bubbles = [];
  final int numberOfBubbles = 10;
  final Color color = Colors.yellow;
  final double maxBubbleSize = 5;

  @override
  void initState() {
    super.initState();

    _createBubles();

    _controller = AnimationController(duration: const Duration(seconds: 1), vsync: this);
    _controller.addListener(_updateBubblePosition);
    _controller.repeat();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => CustomPaint(
        painter: BubblePainter(bubbles: bubbles),
      );

  void _createBubles() {
    var i = numberOfBubbles;
    while (i > 0) {
      bubbles.add(Bubble(
        color: color,
        maxBubbleSize: maxBubbleSize,
      ));
      i--;
    }
  }

  void _updateBubblePosition() {
    bubbles.forEach((it) => it.updatePosition());
    setState(() {});
  }
}

class BubblePainter extends CustomPainter {
  final List<Bubble> bubbles;

  BubblePainter({required this.bubbles});

  @override
  void paint(Canvas canvas, Size size) {
    bubbles.forEach((it) => it.draw(canvas, size));
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => true;
}

class Bubble {
  final double speed = 1;
  final Color color;

  double direction = Random().nextDouble() * 360;

  double radius;

  double x = 0;
  double y = 0;

  Bubble({
    required Color color,
    required double maxBubbleSize,
  })  : color = color.withOpacity(Random().nextDouble()),
        radius = Random().nextDouble() * maxBubbleSize;

  void draw(Canvas canvas, Size size) {
    final paint = Paint()
      ..color = color
      ..strokeCap = StrokeCap.round
      ..style = PaintingStyle.fill;

    canvas.drawCircle(Offset(size.width / 2 + x, y + size.height / 2 - 50), radius, paint);
  }

  void updatePosition() {
    x = Random().nextDouble() * 20 - 10;
    y = Random().nextDouble() * 20 - 20;
  }
}
