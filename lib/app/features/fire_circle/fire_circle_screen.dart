import 'package:flutter/material.dart';
import 'package:kama_test/app/features/fire_circle/fire.dart';
import 'package:kama_test/app/features/fire_circle/circle.dart';

class FireCircleScreen extends StatelessWidget {
  const FireCircleScreen();

  @override
  Widget build(BuildContext context) => Scaffold(
        backgroundColor: Colors.black,
        appBar: AppBar(
          backgroundColor: const Color.fromARGB(255, 121, 15, 15),
          title: const Text('Fire in the circle'),
        ),
        body: const Center(
          child: SizedBox(
            width: 350,
            height: 350,
            child: Circle(
              child: Fire(),
            ),
          ),
        ),
      );
}
