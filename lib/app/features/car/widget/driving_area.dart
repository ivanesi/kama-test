import 'package:kama_test/app/features/car/consts.dart';
import 'package:kama_test/app/features/car/cubit/car_screen_cubit.dart';
import 'package:kama_test/app/features/car/widget/car.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class DrivingArea extends StatelessWidget {
  const DrivingArea({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => Container(
        color: Colors.grey[300],
        height: 350,
        width: drivingAreaWidth,
        child: BlocSelector<CarScreenCubit, CarScreenState, double>(
          selector: (state) => state.carPosition,
          builder: (context, carPosition) => Car(
            position: carPosition,
          ),
        ),
      );
}
