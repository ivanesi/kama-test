import 'package:kama_test/app/features/car/cubit/car_screen_cubit.dart';
import 'package:kama_test/app/features/car/widget/car_controller_widget.dart';
import 'package:kama_test/app/features/car/widget/driving_area.dart';
import 'package:kama_test/app/init/sl.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fps_widget/fps_widget.dart';

class CarScreen extends StatelessWidget {
  const CarScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => BlocProvider<CarScreenCubit>(
        create: (context) => sl(),
        child: const _CarScreen(),
      );
}

class _CarScreen extends StatelessWidget {
  const _CarScreen();

  @override
  Widget build(BuildContext context) => WillPopScope(
        onWillPop: () => context.read<CarScreenCubit>().exitScreen(),
        child: Scaffold(
          appBar: AppBar(
            title: const Text('Speedy car'),
          ),
          body: FPSWidget(
            child: Column(
              children: const [
                SizedBox(height: 10),
                DrivingArea(),
                CarControllerWidget(),
              ],
            ),
          ),
          floatingActionButton: BlocSelector<CarScreenCubit, CarScreenState, int>(
            selector: (state) => state.activeButtonIndex,
            builder: (context, activeButtonIndex) => Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                ElevatedButton(
                  onPressed: activeButtonIndex == -1 ? null : () => context.read<CarScreenCubit>().throttle(),
                  child: const Text('Throttle'),
                ),
                FloatingActionButton(
                  onPressed: () => context.read<CarScreenCubit>().toggleDrive(),
                  child: Icon(activeButtonIndex == -1 ? Icons.play_arrow : Icons.stop),
                ),
              ],
            ),
          ),
        ),
      );
}
