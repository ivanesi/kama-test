import 'package:kama_test/app/features/car/cubit/car_screen_cubit.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class CarControllerWidget extends StatelessWidget {
  const CarControllerWidget();

  @override
  Widget build(BuildContext context) => Container(
        padding: const EdgeInsets.all(10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            ...List.generate(
              4,
              (index) => GestureDetector(
                onTap: () => context.read<CarScreenCubit>().onCarControllerButtonPressed(index),
                child: BlocSelector<CarScreenCubit, CarScreenState, int>(
                  selector: (state) => state.activeButtonIndex,
                  builder: (context, activeButtonIndex) => Container(
                    color: activeButtonIndex == index ? Colors.blue[200] : Colors.blue,
                    width: 40,
                    height: 40,
                    child: Center(
                      child: Text(
                        '${index + 1}',
                        style: const TextStyle(color: Colors.white),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      );
}
