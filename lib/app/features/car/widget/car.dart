import 'package:kama_test/app/features/car/consts.dart';
import 'package:flutter/material.dart';

class Car extends StatelessWidget {
  final double position;

  const Car({
    required this.position,
  });

  @override
  Widget build(BuildContext context) => Align(
        alignment: Alignment.centerLeft,
        child: Container(
          margin: EdgeInsets.only(left: position),
          width: carWidth,
          height: 30,
          color: Colors.purple,
        ),
      );
}
