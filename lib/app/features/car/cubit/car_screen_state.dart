part of 'car_screen_cubit.dart';

@immutable
class CarScreenState {
  final int activeButtonIndex;
  final double carPosition;

  const CarScreenState({
    required this.activeButtonIndex,
    required this.carPosition,
  });

  factory CarScreenState.initial() => const CarScreenState(
        activeButtonIndex: -1,
        carPosition: 0,
      );

  CarScreenState copyWith({
    int? activeButtonIndex,
    double? carPosition,
  }) =>
      CarScreenState(
        activeButtonIndex: activeButtonIndex ?? this.activeButtonIndex,
        carPosition: carPosition ?? this.carPosition,
      );
}
