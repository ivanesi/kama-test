import 'dart:async';
import 'dart:math';

import 'package:kama_test/app/features/car/car_controller.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/scheduler.dart';
import 'package:kama_test/app/presentation/router/app_router.dart';
import 'package:meta/meta.dart';

part 'car_screen_state.dart';

class CarScreenCubit extends Cubit<CarScreenState> {
  final _carController = CarController();
  final AppRouter _router;

  CarScreenCubit(
    this._router,
  ) : super(CarScreenState.initial());

  StreamSubscription<double>? _carPositionSubscription;

  Future<void> onCarControllerButtonPressed(int index) async {
    emit(state.copyWith(activeButtonIndex: index));

    final speed = index + 1;
    _carController.setSpeed(speed);

    _carPositionSubscription ??= _carController.positionStream.listen(_onPositionCalculated);
  }

  void toggleDrive() {
    if (state.activeButtonIndex == -1) {
      onCarControllerButtonPressed(0);
      return;
    }
    onCarControllerButtonPressed(-1);
  }

  int throttledFramesCount = 0;

  void throttle() {
    final s = Stopwatch();
    final r = Random();
    s.start();

    final duration = r.nextInt(30) + 10;

    while (s.elapsedMilliseconds < duration) {}
    s.reset();
    s.stop();

    if (throttledFramesCount > 7) {
      throttledFramesCount = 0;
      return;
    }

    throttledFramesCount++;

    SchedulerBinding.instance.addPostFrameCallback((timeStamp) {
      throttle();
    });
  }

  @override
  Future<void> close() {
    _carPositionSubscription?.cancel();
    _carController.killIsolate();
    return super.close();
  }

  Future<bool> exitScreen() async {
    _router.newRootScreen(Routes.welcomeScreen);
    return false;
  }

  void _onPositionCalculated(double position) => emit(state.copyWith(carPosition: position));
}
