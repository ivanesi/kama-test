import 'dart:async';
import 'dart:isolate';

import 'package:kama_test/app/features/car/consts.dart';

double _position = 0;
bool _moveFront = true;
const double _maxPosition = drivingAreaWidth - carWidth;
StreamSubscription<double>? _speedSubscription;

class CarController {
  Stream<bool>? isolateReadyStream;
  SendPort? _portToIsolate;
  ReceivePort? _receivePort;
  StreamSubscription<dynamic>? _receivePortSubscription;
  Isolate? _isolate;

  final positionStreamController = StreamController<double>();
  Stream<double> get positionStream => positionStreamController.stream;

  CarController() {
    _startIsolate();
  }

  Future<void> killIsolate() async {
    _receivePort?.close();
    _isolate?.kill();
    await _speedSubscription?.cancel();
    await _receivePortSubscription?.cancel();
  }

  Future<void> _startIsolate() async {
    final receivePort = ReceivePort();
    _receivePortSubscription = receivePort.listen(_onIsolateMessage);
    _isolate = await Isolate.spawn(_isolateFunc, receivePort.sendPort);
  }

  void setSpeed(int speed) {
    if (_portToIsolate == null) {
      throw 'isolate is not ready to receive messages';
    }

    _portToIsolate?.send(speed);
  }

  static void _isolateFunc(SendPort sendPort) {
    final receivePort = ReceivePort();

    receivePort.listen((dynamic msg) => _onSpeedMessage(msg, sendPort));
    sendPort.send(receivePort.sendPort);
  }

  static Future<void> _onSpeedMessage(dynamic msg, SendPort sendPort) async {
    print('isolate: speed $msg received');

    if (msg is int) {
      final speed = msg;
      final isolatePositionStream =
          Stream<double>.periodic(const Duration(milliseconds: 16), (_) => _calculateNextPosition(speed));
      isolatePositionStream.takeWhile((_) => speed != 0);

      await _speedSubscription?.cancel();
      _speedSubscription = isolatePositionStream.listen((position) {
        sendPort.send(position);
      });
    }
  }

  void _onIsolateMessage(dynamic msg) {
    if (msg is SendPort) {
      _portToIsolate = msg;
      isolateReadyStream = Stream.value(true);
    }
    if (msg is double) {
      positionStreamController.add(msg);
    }
  }

  static double _calculateNextPosition(int speed) {
    if (_moveFront) {
      _position = _position + speed * 3;
    } else {
      _position = _position - speed * 3;
    }

    if (_position >= _maxPosition) {
      _position = _maxPosition;
      _moveFront = false;
    }

    if (_position <= 0) {
      _position = 0;
      _moveFront = true;
    }

    return _position;
  }
}
