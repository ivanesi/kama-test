import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:kama_test/app/features/welcome/cubit/welcome_screen_cubit.dart';
import 'package:kama_test/app/init/sl.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen();

  @override
  Widget build(BuildContext context) => BlocProvider<WelcomeScreenCubit>(
        create: (context) => sl(),
        child: const _WelcomeScreen(),
      );
}

class _WelcomeScreen extends StatefulWidget {
  const _WelcomeScreen();

  @override
  State<_WelcomeScreen> createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<_WelcomeScreen> {
  double _angle = 0;

  void _rotate() {
    setState(() {
      _angle += 1;
    });
  }

  @override
  Widget build(BuildContext context) => AnimatedRotation(
        duration: const Duration(seconds: 3),
        turns: _angle,
        child: Scaffold(
          body: Center(
              child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    onPressed: () => context.read<WelcomeScreenCubit>().onCarPressed(),
                    child: const Text('Car'),
                  ),
                  const SizedBox(width: 30),
                  ElevatedButton(
                    onPressed: () => context.read<WelcomeScreenCubit>().onFirePressed(),
                    child: const Text('Fire'),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  ElevatedButton(
                    onPressed: _rotate,
                    child: const Text('Rotate'),
                  ),
                  const SizedBox(width: 30),
                  ElevatedButton(
                    onPressed: () => context.read<WelcomeScreenCubit>().onExitPressed(),
                    child: const Text('Quit'),
                  ),
                ],
              ),
            ],
          )),
        ),
      );
}
