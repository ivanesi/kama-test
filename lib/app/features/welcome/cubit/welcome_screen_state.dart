part of 'welcome_screen_cubit.dart';

@immutable
class WelcomeScreenState {
  const WelcomeScreenState();

  factory WelcomeScreenState.initial() => const WelcomeScreenState();
}
