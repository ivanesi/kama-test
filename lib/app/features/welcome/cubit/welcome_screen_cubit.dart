import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kama_test/app/presentation/router/app_router.dart';

part 'welcome_screen_state.dart';

class WelcomeScreenCubit extends Cubit<WelcomeScreenState> {
  final AppRouter _router;

  WelcomeScreenCubit(
    this._router,
  ) : super(WelcomeScreenState.initial());

  void onCarPressed() => _router.push(Routes.carScreen);

  void onFirePressed() => _router.push(Routes.fireCircleScreen);

  Future<void> onExitPressed() async {
    await _router.openAlertDialog(
      dialog: AlertDialog(
        title: const Text('Are you sure?'),
        actions: [
          TextButton(
            onPressed: () => SystemChannels.platform.invokeMethod<void>('SystemNavigator.pop'),
            child: const Text('Sure'),
          ),
          TextButton(
            onPressed: _router.pop,
            child: const Text('Cancel'),
          ),
        ],
      ),
    );
  }
}
