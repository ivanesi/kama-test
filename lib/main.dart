import 'package:kama_test/app/init/initializer.dart';

Future<void> main() async {
  final initializer = Initializer();
  await initializer.init();
  initializer.launcher.run();
}
